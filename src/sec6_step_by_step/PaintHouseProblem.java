package sec6_step_by_step;

import java.util.TreeSet;

/**
 * This is a class where @author Yesid developed an approach that works to solve
 * the problem of painting the houses. Both approaches are top-down, one is just
 * based on recursions and the other is using caching.
 * 
 * @author yesid
 * 
 */
public class PaintHouseProblem {

	static int naiveCalls = 0;
	static int memoizeCalls = 0;
	
	public static void main(String[] args) {
		int[][] cost = { { 17, 2, 17 }, { 16, 16, 5 }, { 14, 3, 9 }, { 3, 1, 2 } };
		System.out.println("Naive approach, total cost:"+paintHousesNaive(cost.length, cost, -1)+" Calls: "+naiveCalls);
		int[][] cache = new int[cost.length][cost[0].length];
		System.out.println("Memoize approach, total cost:"+paintHousesMemoize(cost.length, cost, -1, cache)+" Calls: "+memoizeCalls);
	}

	/**
	 * This solves the problem of painting the houses in the cheapest way with no
	 * consecutive house of the same colour
	 * 
	 * @param i-nth house element
	 * @param cost the colour cost
	 * @param selCol the current colour selection
	 * @return the cost of painting the houses
	 */
	private static int paintHousesNaive(int i, int[][] cost, int selCol) {
		naiveCalls++;
		if (i <= 0) {
			return 0;
		}

//		int hc1 = Integer.MAX_VALUE;
//		int hc2 = Integer.MAX_VALUE;
//		int hc3 = Integer.MAX_VALUE;
//		if(selCol!=0)
//			hc1 = cost[i-1][0] + paintHousesNaive(i-1, cost, 0);
//		if(selCol!=1)
//			hc2 = cost[i-1][1] + paintHousesNaive(i-1, cost, 1);
//		if(selCol!=2)
//			hc3 = cost[i-1][2] + paintHousesNaive(i-1, cost, 2);

		TreeSet<Integer> cheapest = new TreeSet<Integer>();

		for (int color = 0; color < cost[0].length; color++) {
			if (selCol != color) {
				int currentCost = cost[i-1][color] + paintHousesNaive(i-1, cost, color);
				cheapest.add(currentCost);
			}
		}
//		cheapest.add(hc1);
//		cheapest.add(hc2);
//		cheapest.add(hc3);
		return cheapest.first();
	}
	
	/**
	 * Memoiza approach
	 * @param i
	 * @param cost
	 * @param selCol
	 * @param cache
	 * @return
	 */
	private static int paintHousesMemoize(int i, int[][] cost, int selCol, int[][] cache) {
		
		memoizeCalls++;
		if (i <= 0) {
			return 0;
		}

		TreeSet<Integer> cheapest = new TreeSet<Integer>();

		for (int color = 0; color < cost[0].length; color++) {
			if (selCol != color) {
				TreeSet<Integer> ind = new TreeSet<Integer>();
				ind.add(0);
				ind.add(1);
				ind.add(2);
				ind.remove(selCol);
				//This "if" is required cus it has to check, according to the cache of the previous level,
				//decide which one of the two options available, is the best.
				if(cache[i-1][ind.first()] !=0 && cache[i-1][ind.last()] !=0){
					return Math.min(cache[i-1][ind.pollFirst()], cache[i-1][ind.pollFirst()]);
				}else {
					cache[i-1][color] = cost[i-1][color] + paintHousesMemoize(i-1, cost, color, cache);
					cheapest.add(cache[i-1][color]);
				}
			}
		}

		return cheapest.first();
	}
	
	
	/**
	 * Memoiza approach with answer reconstruction
	 * @param i
	 * @param cost
	 * @param selCol
	 * @param cache
	 * @return
	 */
	private static int paintHousesMemoizeTrackSol(int i, int[][] cost, int selCol, int[][] cache) {
		memoizeCalls++;
		if (i <= 0) {
			return 0;
		}

		TreeSet<Integer> cheapest = new TreeSet<Integer>();

		for (int color = 0; color < cost[0].length; color++) {
			if (selCol != color) {
				if(cache[i-1][color]!=0){
					return cache[i-1][color];
				}else {
					cache[i-1][color] = cost[i-1][color] + paintHousesMemoizeTrackSol(i-1, cost, color, cache);
					cheapest.add(cache[i-1][color]);
				}
			}
		}

		return cheapest.first();
	}

}
