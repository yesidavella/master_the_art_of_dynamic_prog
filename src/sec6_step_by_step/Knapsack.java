package sec6_step_by_step;

import java.util.Arrays;

public class Knapsack {
	
	public static void main(String[] args) {
        int[] weights = {3, 7, 10, 6};
        int[] values = {4, 14, 10, 5};
        int W = 20;
        int N = weights.length;

        System.out.println("knapsack: "+knapsack(weights, values, W, N-1));
        
        int[][] cache = new int[W+1][N+1];
        for (int i = 0; i <= W; i++) {
            Arrays.fill(cache[i], -1);
        }
        System.out.println("knapsackMemo: "+knapsackMemo(weights, values, W, N-1, cache));
        System.out.println("knapsackDP: "+knapsackDP(weights, values, W));
        
        System.out.println("\nReconstructing the selections: ");
        System.out.println(knapsackDPReconstruction(weights, values, W));
    }

    /*
     * Recursive solution
     */
    public static int knapsack(int[] ws, int[] vs, int W, int i) {
        if (i == -1 || W == 0) {
            return 0;
        }
        if (ws[i] <= W) {
            int include = vs[i] + knapsack(ws, vs, W-ws[i], i-1);
            int exclude = knapsack(ws, vs, W, i-1);
            return Math.max(include, exclude);
        } else {
            return knapsack(ws, vs, W, i-1);
        }
    }

    /*
     * Memoization
     */
    public static int knapsackMemo(int[] weights, int[] values, int W, int i, int[][] cache) {
        if (i == -1 || W == 0) {
            return 0;
        }
        if (cache[W][i] != -1) {
            return cache[W][i];
        }
        if (weights[i] <= W) {
            int include = values[i] + knapsackMemo(weights, values, W-weights[i], i-1, cache);
            int exclude = knapsackMemo(weights, values, W, i-1, cache);
            cache[W][i] = Math.max(include, exclude);
            return cache[W][i];
        } else {
            return cache[W][i] = knapsackMemo(weights, values, W, i-1,cache);
        }
    }

    /*
     * Bottom up approach
     */
    public static int knapsackDP(int[] weights, int[] values, int W) {
        int items = weights.length;
        int[][] cache = new int[W+1][items+1];
        
        for (int i = 1; i <= items; i++) {
            for (int w = 1; w <= W; w++) {
                
            	if (weights[i-1] <= w) {
            		int including = values[i-1] + cache[ w-weights[i-1] ][i-1];
            		int excluding = cache[w][i-1];
                    cache[w][i] = Math.max(including, excluding);
                } else {
                    cache[w][i] = cache[w][i-1];
                }
            }
        }
        return cache[W][items];
    }


    /*
     * Bottom up approach with reconstruction of the solution
     */
    public static int knapsackDPReconstruction(int[] weights, int[] values, int W) {
        int N = weights.length;
        int[][] dp = new int[W+1][N+1];
        boolean[][] decisions = new boolean[W+1][N+1];
        
        for (int i = 1; i <= N; i++) {
            for (int w = 1; w <= W; w++) {
                
            	if (weights[i-1] <= w) {
            		//This if is important to replace the Math.max(v1,v2)
                    if (dp[ w-weights[i-1] ][i-1] + values[i-1] > dp[w][i-1]) {
                        decisions[w][i] = true;
                        dp[w][i] = dp[ w-weights[i-1] ][i-1] + values[i-1];
                    } else {
                        dp[w][i] = dp[w][i-1];
                    }
                } else {
                    dp[w][i] = dp[w][i-1];
                }
            }
        }
        
        int i = N;
        int w = W;
        while (i>0 && w>0) {
            boolean picked = decisions[w][i];
            if (picked) {
                System.out.println("Picked: " + (i-1) + ", Weight: " + weights[i-1] + ", Value: " + values[i-1]);
                w -= weights[i-1];
                i--;
            } else {
                i--;
            }
        }
        return dp[W][N];
    }
	
}