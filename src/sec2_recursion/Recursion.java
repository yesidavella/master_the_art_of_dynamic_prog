package sec2_recursion;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class Recursion {
	
	public boolean checkNumbersInSeq(int[] nums) {

		int i = 0;
		
		if(nums.length == 0) {
			return false;
		}
		
		return seq(nums, i) == nums[i];
	}
	
	public int seq(int[] nums, int i) {
		
		if(nums.length == i+1) {
			return nums[i];
		}
		
		if(nums[i]+1 == seq(nums, i+1)){
			return nums[i];
		}
		
		return Integer.MIN_VALUE;
	}
	
	@Test
	public void testConsecutiveDigits() {
		
		int[] seqNumbs = {-1,0,1,2,3,4,5,6};
		int[] nonSeqNumbs = {-1,0,1,2,3,5,5,6};
		int[] negNumbs = {-10,-9,-8,-7,-6};
		
		assertTrue(checkNumbersInSeq(seqNumbs));
		assertFalse(checkNumbersInSeq(nonSeqNumbs));
		assertTrue(checkNumbersInSeq(negNumbs));
	}
	
	///Exercise 2. Sum digits
	
	public int sumDigits(int number) {
		String num = Integer.toString(number);		
		return recursiveSum(num, 0);
	}
	
	public int recursiveSum(String num, int i) {
		
		if(i+1 == num.length()) {
			return Integer.valueOf(Character.toString(num.charAt(i)));
		}
		
		return Integer.valueOf(Character.toString(num.charAt(i))) + recursiveSum(num, i+1);
	}
	
	@Test
	public void testSumDigits() {
		
		int number1 = 123;
		int number2 = 1203;
		int number3 = 102030;
		
		assertEquals(6, sumDigits(number1));
		assertEquals(6, sumDigits(number2));
		assertEquals(6, sumDigits(number3));
	}
	
	
}
