package sec4_intro_dyn_prog;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BinomialCoeficient {

	@Test
	public void testGetBinomialCoeficientTopDown() {
		assertEquals(3, getNaiveTopDownBinomialCoef(3, 2));
		assertEquals(10, getNaiveTopDownBinomialCoef(5, 2));
		assertEquals(28, getNaiveTopDownBinomialCoef(8, 2));
		assertEquals(4950, getNaiveTopDownBinomialCoef(100, 2));
		
		assertEquals(3, getCachedTopDownBinomialCoef(3, 2, null));
		assertEquals(10, getCachedTopDownBinomialCoef(5, 2, null));
		assertEquals(28, getCachedTopDownBinomialCoef(8, 2, null));
		assertEquals(4950, getCachedTopDownBinomialCoef(100, 2, null));
	}

	public int getNaiveTopDownBinomialCoef(int n, int k) {

		if (n == k || k == 0) {
			return 1;
		}

		return getNaiveTopDownBinomialCoef(n - 1, k - 1) + getNaiveTopDownBinomialCoef(n - 1, k);
	}

	public int getCachedTopDownBinomialCoef(int n, int k, int[][] cache) {

		if (cache == null) {
			cache = new int[n + 1][k + 1];
		}

		if (n == k || k == 0) {
			//cache[n][k] = 1;
			return 1;
		}
		
		if(cache[n-1][k-1] == 0) {
			cache[n-1][k-1] = getCachedTopDownBinomialCoef(n-1, k-1, cache);
		}
		
		if(cache[n-1][k] == 0) {
			cache[n-1][k] = getCachedTopDownBinomialCoef(n-1, k, cache);
		}

		return cache[n-1][k-1] + cache[n-1][k];
	}
	
	
	@Test
	public void testGetBinomialCoefficientButtonUp() {
		assertEquals(28, getBinomialCoefWithButtonUpApproach(8, 2));
		assertEquals(3, getBinomialCoefWithButtonUpApproach(3, 2));
		assertEquals(4950, getBinomialCoefWithButtonUpApproach(100, 2));
	}

	
	private int getBinomialCoefWithButtonUpApproach(int n, int k) {
		
		int[][] cache = new int[n+1][k+1];
		
		//Setting-up basic cases when C(n,n)=1 and C(n,0)=1 
		for(int i=1; i<cache.length; i++) {
			cache[i][0] = 1;
			
			if(i <= k) {
				cache[i][i] = 1;
			}
		}
		
		//Creating (filling-up) the matrix from n=2, k=1 until n,k with values
		for(int i=2; i<=n; i++) {
			for(int j=1; j<n; j++) {
				
				if(j <= k) {
					cache[i][j] = cache[i-1][j-1] + cache[i-1][j];
				}
			}
		}
		
		return cache[n][k];
	}

}
