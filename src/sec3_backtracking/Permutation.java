package sec3_backtracking;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public class Permutation {

	static int counter = 0;

	/**
	 * Returns all the permutations of elements of the given array
	 * 
	 * @param input
	 */
	public void permutation(int[] input) {
		Arrays.sort(input);
		permutation(input, new ArrayList<>(), new boolean[input.length]);
	}

	/**
	 * Returns all the permutations of elements of the given array
	 * 
	 * @param input
	 * @param partial contains the partial permutation
	 * @param used    uses flags to indicate whether or not the element at the given
	 *                index is used or not
	 */
	public static void permutation(int[] input, ArrayList<Integer> partial, boolean[] used) {
		if (partial.size() == input.length) {
			counter++;
			System.out.println("Counter:" + counter + ". " + Arrays.toString(partial.toArray()));
			return;
		}

		for (int i = 0; i < input.length; i++) {
			if (!used[i]) {
				used[i] = true;
				partial.add(input[i]);
				permutation(input, partial, used);
				used[i] = false;
				partial.remove(partial.size() - 1);
			}
		}
	}

	@Test
	public void testPermutation() {
		int[] input = { 3, 2, 4, 1 };
		permutation(input);
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	@Test
	public void testPermutationYesidVersion() {
		int[] input = { 3, 2, 4, 1 };
		permutationYesidVers(input);
	}

	private void permutationYesidVers(int[] input) {
		Arrays.sort(input);
		Integer counter = 0;
		permutationYesidVers(input, new ArrayList<Integer>(), new boolean[input.length],counter);
		
	}

	private int permutationYesidVers(int[] input, ArrayList<Integer> partial, boolean[] usado, Integer counter) {
		
		if(partial.size() == input.length) {
			//counter +=1;
			System.out.println("Counter:"+ ++counter+" "+partial.toString());
			return counter;
		}
		
		for(int i=0; i<input.length; i++ ) {
			
			if(!usado[i]) {
				usado[i]=true;
				partial.add(input[i]);
				counter = permutationYesidVers(input, partial, usado, counter);
				partial.remove(partial.size()-1);
				usado[i]=false;
			}
		}
		return counter;
		
	}
}
