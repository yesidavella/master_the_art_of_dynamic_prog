package sec3_backtracking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class MultipleSentences {

	@Test
	public void testSentences() {
		String sentence = "catsanddog";
		Set<String> dicc = new HashSet<String>();
		dicc.add("cat");
		dicc.add("cats");
		dicc.add("and");
		dicc.add("sand");
		dicc.add("dog");
		
		getSentences(sentence, dicc, new ArrayList<String>(), 1);
	}

	private void getSentences(String input, Set<String> dicc, List<String> partialSol, int level) {

		if(input.length() == 0) {
			System.out.println("Level: "+level+" "+partialSol.toString());
			return;
		}
		
		for(int i=1; i<=input.length(); i++) {
			
			String option = input.substring(0, i);
			
			if(dicc.contains(option)) {
				partialSol.add(option);
				level++;
				getSentences(input.substring(i), dicc, partialSol, level);
				level--;
				partialSol.remove(option);
			}
		}
	}
}