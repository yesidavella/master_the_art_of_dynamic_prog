package sec3_backtracking;

import java.util.ArrayList;

import org.junit.Test;

public class AnagramExercise {

	@Test
	public void testAnagrams() {
		String word = "abcd";
		createAnagram(word, new ArrayList<Character>(), new boolean[word.length()]);
	}

	private void createAnagram(String input, ArrayList<Character> partial, boolean[] used) {
		
		if(partial.size() == input.length()) {
			System.out.println(partial.toString());
		}
		
		for(int i=0; i<input.length();i++) {
			
			if(!used[i]) {
				used[i]=true;
				partial.add(input.charAt(i));
				createAnagram(input, partial, used);
				partial.remove(partial.size()-1);
				used[i]=false;
			}
		}
	}
}
