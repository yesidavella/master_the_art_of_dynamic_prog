package stairwarOptions_problem;

import java.util.Arrays;

import org.junit.Test;

public class StairwayOptions {

	static long totalCallsNaiveAppr = 0;
	static int totalCallsMemoizationAppr = 0;

	public static void main(String[] args) {
		int stairwaySize = 40;

		System.out.println("The total options of the memoization approach is: "
				+ memoizationApproach(stairwaySize, new long[stairwaySize + 1]) + " with total calls:"
				+ totalCallsMemoizationAppr);
		System.out.println("The total options of the botton-up approach is: " + bottomUpApproach(stairwaySize));
		System.out.println("The total options of the naive approach is: " + naiveApproach(stairwaySize)
				+ " with total calls:" + totalCallsNaiveAppr);
	}

	/**
	 * This is the naive approach, using ONLY recursion to find the solutions
	 * 
	 * @param stairwaySize the steps the stairway has
	 * @return the total possible options one person can go down through the stair,
	 *         walking 1,2 or 3 steps in all the possible combination.
	 */
	private static long naiveApproach(int stairwaySize) {

		totalCallsNaiveAppr++;
		// Base cases
		if (stairwaySize == 0) {
			return 1;
		} else if (stairwaySize <= 0) {
			return 0;
		}

		return naiveApproach(stairwaySize - 1) + naiveApproach(stairwaySize - 2) + naiveApproach(stairwaySize - 3);
	}
	
	/**
	 * Solving the problem using memoization technique 
	 * @param stairwayLong
	 * @param cache
	 * @return
	 */
	private static long memoizationApproach(int stairwayLong, long[] cache) {
		totalCallsMemoizationAppr++;
		// Base cases
		if (stairwayLong == 0) {
			return 1;
		} else if (stairwayLong <= 0) {
			return 0;
		}

		if (cache[stairwayLong] != 0) {
			return cache[stairwayLong];
		}

		cache[stairwayLong] = memoizationApproach(stairwayLong - 1, cache)
				+ memoizationApproach(stairwayLong - 2, cache) + memoizationApproach(stairwayLong - 3, cache);
		return cache[stairwayLong];
	}

	/**
	 * Using bottom-up strategy also called table-filling
	 * @param stairwaySize
	 * @return
	 */
	private static long bottomUpApproach(int stairwaySize) {

		long[] cache = new long[stairwaySize + 1];
		Arrays.fill(cache, -1);

		// Base case
		cache[0] = 0;
		cache[1] = 1;
		cache[2] = 2;
		cache[3] = 4;

		for (int i = 4; i <= stairwaySize; i++) {
			cache[i] = cache[i - 1] + cache[i - 2] + cache[i - 3];
		}

		return cache[stairwaySize];
	}

	@Test
	public void testBasicValues() {
		org.junit.Assert.assertEquals(1, naiveApproach(1));
		org.junit.Assert.assertEquals(2, naiveApproach(2));
		org.junit.Assert.assertEquals(4, naiveApproach(3));
		org.junit.Assert.assertEquals(7, naiveApproach(4));
		org.junit.Assert.assertEquals(13, naiveApproach(5));
		org.junit.Assert.assertEquals(121415, naiveApproach(20));
	}

}
